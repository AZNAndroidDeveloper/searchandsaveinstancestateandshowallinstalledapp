package uz.azn.lesson29

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListViewAdapter(  context: Context, private val listItems :ArrayList<App>):ArrayAdapter<App>(context,0,listItems) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var listLayout = convertView
        var viewHolder:ViewHolder
        if (listLayout == null){
            val inflatList = (context as Activity).layoutInflater
            listLayout = inflatList!!.inflate(R.layout.items_app,null,false)
            viewHolder = ViewHolder()
            viewHolder.icon = listLayout.findViewById(R.id.icon_image)
            viewHolder.tvApp = listLayout.findViewById(R.id.text)
            viewHolder.tvApp.text = listItems[position].appName
            viewHolder.icon.setImageDrawable(listItems[position].image)

        }
        return listLayout!!
    }
    class ViewHolder(){
      lateinit  var icon :ImageView
         lateinit var tvApp:TextView
    }
}