package uz.azn.lesson29

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import uz.azn.lesson29.databinding.ActivityMain2Binding

class MainActivity2 : AppCompatActivity() {
    val binding by lazy { ActivityMain2Binding.inflate(layoutInflater) }
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        var arraylist = ArrayList<App>()

        val packageManager = this.packageManager
        val intent =Intent(Intent.ACTION_VIEW)
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        val apps = packageManager.queryIntentActivities(intent, PackageManager.PERMISSION_GRANTED)
        for (info:ResolveInfo in apps){
            arraylist.add(
                App(
                    info.activityInfo.applicationInfo.loadIcon(packageManager),
                info.activityInfo.applicationInfo.loadLabel(packageManager).toString()))
        }
        binding.listView.adapter = ListViewAdapter(this,arraylist)
    }
}