package uz.azn.lesson29

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.webkit.WebViewClient
import android.widget.ArrayAdapter
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuItemCompat
import uz.azn.lesson29.databinding.ActivityMainBinding
var increment:Int = 0
lateinit var array :Array<String>
lateinit var adapter: ArrayAdapter<String>

class MainActivity : AppCompatActivity() {
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        array = arrayOf(
            "Kotlin",
            "Android",
            "Kotlin",
            "C++",
            "JavaScrip",
            "Flutter",
            "Swift",
            "objective-C"
        )
        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, array)
        binding.listView.adapter = adapter

       binding.searchView.queryHint = "Qidirish"
        binding.searchView.isIconified = false
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                binding.searchView.clearFocus()
                if (array.contains(query)) {
                    adapter.filter.filter(query)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }

        })

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.search_menu, menu)
        val item = menu!!.findItem(R.id.search)
        val searchView = MenuItemCompat.getActionView(item) as SearchView
        searchView.queryHint = "Qidirish"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus()
                if (array.contains(query)) {
                    adapter.filter.filter(query)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }

        })
        return super.onCreateOptionsMenu(menu)
    }

}
//        binding.btnInc.setOnClickListener {
//            increment++
//            binding.tvCount.text = increment.toString()
//        }
//        binding.btnDec.setOnClickListener {
//            increment--
//            binding.tvCount.text = increment.toString()
//        }






//        searchView.setOnQueryTextFocusChangeListener(object:SearchView.OnQueryTextListener{
//            override fun onQueryTextSubmit(query: String?): Boolean {
//               searchView.clearFocus()
//                if (array.contains(query)){
//                    adapter.filter.filter(query)
//                }
//                return false
//            }
//
//            override fun onQueryTextChange(newText: String?): Boolean {
//               adapter.filter.filter(newText)
//                return false
//            }
//
//
//
//
//        })
//        return super.onCreateOptionsMenu(menu)
//    }

//    override fun onBackPressed() {
//        if (binding.webView.canGoBack()){
//            binding.webView.goBack()
//        }else
//
//        super.onBackPressed()
//    }
// Bu metotdlar ekranlar qayrilganda  ishlaydi
//    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
//        super.onSaveInstanceState(outState, outPersistentState)
//        outState.putInt("counter",increment)
//    }
//
//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//        increment = savedInstanceState.getInt("counter", increment)
//    }
